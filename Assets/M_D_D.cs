﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class M_D_D : MonoBehaviour
{
    public GameObject bloque;
    public float valor;

    void Update()
    {
        StartCoroutine(movimiento());
    }

    IEnumerator movimiento()
    {
        //Debug.Log("Llamado");
        bloque.transform.position = new Vector3(bloque.transform.position.x, bloque.transform.position.y - valor, bloque.transform.position.z);
        if (bloque.transform.position.y <= -37.0f)
        {
            bloque.transform.position = new Vector3(bloque.transform.position.x, 31.0f, bloque.transform.position.z);
        }
        yield return new WaitForSeconds(0.5f);
    }
}
