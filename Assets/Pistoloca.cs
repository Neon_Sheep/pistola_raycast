﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistoloca : MonoBehaviour
{
    public Transform pistola;
    public float range = 0f;
    private int destruidos = 0;

    void Update()
    {
        if(Input.GetButtonDown("Fire1"))
        {
            StartCoroutine("Disparo");
        }
    }

    IEnumerator Disparo()
    {
        
        RaycastHit hit;
        Ray ray = new Ray(pistola.position, transform.forward);
        if (Physics.Raycast(ray,out hit,range))
        {
            if (hit.collider.tag == "Loco")
            {
                Loco loco = hit.collider.GetComponent<Loco>();
                loco.vida -= 1;
                Debug.Log(loco.vida);
                if (loco.vida == 0)
                {
                    destruidos++;
                }
            }
        }

        Debug.DrawRay(pistola.position, transform.forward * range, Color.green);
        yield return null;
    }
}
