﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bloqueo : MonoBehaviour
{
    public GameObject bloque;
    public float valor;
    //private Vector3 inicial = new Vector3(bloque.transform.position.x, bloque.transform.position.y, bloque.transform.position.z);

    void Update()
    {
        StartCoroutine(movimiento());
    }
    //Cambio2
    IEnumerator movimiento()
    {
        //Debug.Log("Llamado");
        bloque.transform.position = new Vector3(bloque.transform.position.x+valor, bloque.transform.position.y, bloque.transform.position.z);
        if (bloque.transform.position.x >= 90.0)
        {
            bloque.transform.position = new Vector3(-90.0f, bloque.transform.position.y, bloque.transform.position.z);
        } 
        yield return new WaitForSeconds(0.5f);
    }
}
