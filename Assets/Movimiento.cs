﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    public GameObject cubo;
    public float valor;
    public Transform camara;
    public float distancia = 10f;

    void Update()
    {

        if(Input.GetAxis("Mouse X") < 0)//Izquierda
        {
            cubo.transform.Rotate(Vector3.down * valor * Time.deltaTime);
        }
        
        if(Input.GetAxis("Mouse X") > 0)//Derecha
        {
            cubo.transform.Rotate(Vector3.up * valor * Time.deltaTime);
        }
        
        if(Input.GetAxis("Mouse Y") < 0)//Arriba
        {
            cubo.transform.Rotate(Vector3.right * valor * Time.deltaTime);
        }
        
        if(Input.GetAxis("Mouse Y") > 0)//Abajo
        {
            cubo.transform.Rotate(Vector3.left * valor * Time.deltaTime);
        }

        Vector3 posicion = camara.position + cubo.transform.forward * distancia;
        transform.position = posicion;

        /* if (Input.GetKey(KeyCode.LeftArrow))
         {
             cubo.transform.Rotate(Vector3.up * valor * Time.deltaTime);
         } 

         if (Input.GetKey(KeyCode.RightArrow))
         {
             cubo.transform.Rotate(-Vector3.up * valor * Time.deltaTime);
         }     */
    }
}
